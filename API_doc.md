# Backend API Documentation

This document provides all the necessary details for frontend developers to integrate with the backend API.

## Base URL

The base URL for all API endpoints is `https://38-api.app.softgen.ai/`

## API Endpoints

### Phone Management

#### List Phones

- **GET** `https://38-api.app.softgen.ai/api/phones`
  - Retrieves a list of phones.
  - Query parameters can be used to filter results.
  - **Response:**
    - Status code: 200 OK
    - Content: Array of phone objects

#### Fetch Phone

- **GET** `https://38-api.app.softgen.ai/api/phones/:id`
  - Retrieves a single phone by its ID.
  - **Path Parameters:**
    - `id`: The ID of the phone to retrieve.
  - **Response:**
    - Status code: 200 OK / 404 Not Found
    - Content: Phone object / Error message

#### Add Phone

- **POST** `https://38-api.app.softgen.ai/api/phones`
  - Adds a new phone to the database.
  - **Request Body:** JSON object containing phone details.
  - **Response:**
    - Status code: 201 Created
    - Content: Newly created phone object

#### Edit Phone

- **PUT** `https://38-api.app.softgen.ai/api/phones/:id`
  - Updates an existing phone's details.
  - **Path Parameters:**
    - `id`: The ID of the phone to update.
  - **Request Body:** JSON object containing updated phone details.
  - **Response:**
    - Status code: 200 OK
    - Content: Updated phone object

#### Remove Phone

- **DELETE** `https://38-api.app.softgen.ai/api/phones/:id`
  - Deletes a phone from the database.
  - **Path Parameters:**
    - `id`: The ID of the phone to delete.
  - **Response:**
    - Status code: 200 OK
    - Content: Success message

## Authentication

No authentication is required to access these endpoints.

## Error Handling

The API uses standard HTTP response codes to indicate the success or failure of an API request. In general:

- Codes in the `2xx` range indicate success.
- Codes in the `4xx` range indicate an error that failed given the information provided (e.g., a required parameter was omitted, a phone with the given ID was not found, etc.).
- Codes in the `5xx` range indicate an error with our server.

## Examples

### List Phones

```bash
curl -X GET 'https://38-api.app.softgen.ai/api/phones'
```

### Fetch Phone

```bash
curl -X GET 'https://38-api.app.softgen.ai/api/phones/1'
```

### Add Phone

```bash
curl -X POST 'https://38-api.app.softgen.ai/api/phones' -H 'Content-Type: application/json' -d '{"brand":"Apple","model":"iPhone 13","specifications":{"color":"Midnight","storage":"128GB","battery":"3240mAh"},"price":799.00}'
```

### Edit Phone

```bash
curl -X PUT 'https://38-api.app.softgen.ai/api/phones/1' -H 'Content-Type: application/json' -d '{"price":899.00}'
```

### Remove Phone

```bash
curl -X DELETE 'https://38-api.app.softgen.ai/api/phones/1'
```

Please note that the examples provided are for illustration purposes only and the actual request and response may vary based on the current state of the database and the specific details provided in the request.