const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

async function createPhone(phoneData) {
  return await prisma.phone.create({
    data: phoneData,
  });
}

async function updatePhone(id, phoneData) {
  const parsedId = parseInt(id, 10);
  return await prisma.phone.update({
    where: { id: parsedId },
    data: phoneData,
  });
}

async function deletePhone(id) {
  const parsedId = parseInt(id, 10);
  return await prisma.phone.delete({
    where: { id: parsedId },
  });
}

async function getPhones(filterOptions) {
  return await prisma.phone.findMany({
    where: filterOptions,
  });
}

async function getPhoneById(id) {
  return await prisma.phone.findUnique({
    where: { id: parseInt(id, 10) },
  });
}

module.exports = { createPhone, updatePhone, deletePhone, getPhones, getPhoneById };