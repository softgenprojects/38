const { createPhone, updatePhone, deletePhone, getPhones, getPhoneById } = require('@services/phoneService');

async function addPhone(req, res) {
  try {
    const phone = await createPhone(req.body);
    res.status(201).json(phone);
  } catch (error) {
    res.status(500).json({ message: 'Error adding phone', error });
  }
}

async function editPhone(req, res) {
  try {
    if (typeof req.body.price !== 'number') {
      return res.status(400).json({ message: 'Invalid price format' });
    }
    const phone = await updatePhone(req.params.id, req.body);
    res.status(200).json(phone);
  } catch (error) {
    res.status(500).json({ message: 'Error updating phone', error });
  }
}

async function removePhone(req, res) {
  try {
    await deletePhone(req.params.id);
    res.status(200).json({ message: 'Phone deleted successfully' });
  } catch (error) {
    res.status(500).json({ message: 'Error deleting phone', error });
  }
}

async function listPhones(req, res) {
  try {
    const filterOptions = req.query;
    const phones = await getPhones(filterOptions);
    res.status(200).json(phones);
  } catch (error) {
    res.status(500).json({ message: 'Error retrieving phones', error });
  }
}

async function fetchPhone(req, res) {
  try {
    const phone = await getPhoneById(req.params.id);
    if (phone) {
      res.status(200).json(phone);
    } else {
      res.status(404).json({ message: 'Phone not found' });
    }
  } catch (error) {
    res.status(500).json({ message: 'Error fetching phone', error });
  }
}

module.exports = { addPhone, editPhone, removePhone, listPhones, fetchPhone };
