const express = require('express');
const { addPhone, editPhone, removePhone, listPhones, fetchPhone } = require('@controllers/phoneController');

const router = express.Router();

router.post('/', addPhone);
router.put('/:id', editPhone);
router.delete('/:id', removePhone);
router.get('/', listPhones);
router.get('/:id', fetchPhone);

module.exports = router;