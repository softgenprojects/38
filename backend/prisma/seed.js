const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

async function main() {
  // Seed data for Phone model
  const phones = [
    {
      brand: 'Apple',
      model: 'iPhone 13',
      specifications: {
        color: 'Midnight',
        storage: '128GB',
        battery: '3240mAh'
      },
      price: 799.00
    },
    {
      brand: 'Samsung',
      model: 'Galaxy S21',
      specifications: {
        color: 'Phantom Gray',
        storage: '256GB',
        battery: '4000mAh'
      },
      price: 699.99
    },
    {
      brand: 'Google',
      model: 'Pixel 6',
      specifications: {
        color: 'Stormy Black',
        storage: '128GB',
        battery: '4614mAh'
      },
      price: 599.00
    },
    // Add more phone entries here
  ];

  for (const phone of phones) {
    await prisma.phone.create({
      data: phone,
    });
  }
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });