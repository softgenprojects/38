import axios from 'axios';

export const listPhones = async () => {
  try {
    const response = await axios.get('https://38-api.app.softgen.ai/api/phones');
    return response.data;
  } catch (error) {
    console.error('Error fetching phones:', error);
    throw error;
  }
};

export const getPhoneById = async (id) => {
  try {
    const response = await axios.get(`https://38-api.app.softgen.ai/api/phones/${id}`);
    return response.data;
  } catch (error) {
    console.error('Error fetching phone by ID:', error);
    throw error;
  }
};

export const addPhone = async (phoneData) => {
  try {
    const response = await axios.post('https://38-api.app.softgen.ai/api/phones', phoneData);
    return response.data;
  } catch (error) {
    console.error('Error adding new phone:', error);
    throw error;
  }
};

export const editPhone = async (id, phoneData) => {
  try {
    const response = await axios.put(`https://38-api.app.softgen.ai/api/phones/${id}`, phoneData);
    return response.data;
  } catch (error) {
    console.error('Error editing phone:', error);
    throw error;
  }
};

export const removePhone = async (id) => {
  try {
    const response = await axios.delete(`https://38-api.app.softgen.ai/api/phones/${id}`);
    return response.data;
  } catch (error) {
    console.error('Error removing phone:', error);
    throw error;
  }
};