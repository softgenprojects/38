import { useQuery, useMutation, useQueryClient } from 'react-query';
import { listPhones, addPhone, editPhone, removePhone } from '@api/PhonesApi';

export const usePhones = () => {
  const queryClient = useQueryClient();

  const { data: phones, ...queryRest } = useQuery('phones', listPhones, {
    onError: (error) => console.error('Error fetching phones:', error)
  });

  const addMutation = useMutation(addPhone, {
    onSuccess: () => {
      queryClient.invalidateQueries('phones');
    },
    onError: (error) => console.error('Error adding phone:', error)
  });

  const updateMutation = useMutation(({ id, phoneData }) => editPhone(id, phoneData), {
    onSuccess: () => {
      queryClient.invalidateQueries('phones');
    },
    onError: (error) => console.error('Error updating phone:', error)
  });

  const deleteMutation = useMutation(removePhone, {
    onSuccess: () => {
      queryClient.invalidateQueries('phones');
    },
    onError: (error) => console.error('Error deleting phone:', error)
  });

  return {
    phones,
    addPhone: addMutation.mutate,
    updatePhone: updateMutation.mutate,
    deletePhone: deleteMutation.mutate,
    ...queryRest,
  };
};