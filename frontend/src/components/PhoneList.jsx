import { useState } from 'react';
import { Box, Input, Stack } from '@chakra-ui/react';
import { PhoneCard } from '@components/PhoneCard';
import { usePhones } from '@hooks/usePhones';

export const PhoneList = () => {
  const { phones } = usePhones();
  const [searchTerm, setSearchTerm] = useState('');

  const filteredPhones = phones?.filter(phone =>
    phone.brand.toLowerCase().includes(searchTerm.toLowerCase()) ||
    phone.model.toLowerCase().includes(searchTerm.toLowerCase()) ||
    phone.specifications.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return (
    <Box>
      <Input
        placeholder='Search for phones...'
        my={4}
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
      />
      <Stack spacing={4}>
        {filteredPhones?.map(phone => (
          <PhoneCard key={phone.id} phone={phone} />
        ))}
      </Stack>
    </Box>
  );
};
