import React from 'react';
import { FormControl, FormLabel, Input, Button, Box } from '@chakra-ui/react';
import { usePhones } from '@hooks/usePhones';

export const PhoneForm = ({ phoneData, onClose }) => {
  const { addPhone, updatePhone } = usePhones();
  const isEditing = Boolean(phoneData);
  const [formData, setFormData] = React.useState({
    brand: phoneData?.brand || '',
    model: phoneData?.model || '',
    specifications: phoneData?.specifications || '',
    price: phoneData?.price || ''
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (isEditing) {
      updatePhone({ id: phoneData.id, phoneData: formData });
    } else {
      addPhone(formData);
    }
    onClose();
  };

  return (
    <Box as='form' onSubmit={handleSubmit} p={4}>
      <FormControl isRequired>
        <FormLabel htmlFor='brand'>Brand</FormLabel>
        <Input id='brand' name='brand' value={formData.brand} onChange={handleChange} />
      </FormControl>
      <FormControl isRequired mt={4}>
        <FormLabel htmlFor='model'>Model</FormLabel>
        <Input id='model' name='model' value={formData.model} onChange={handleChange} />
      </FormControl>
      <FormControl mt={4}>
        <FormLabel htmlFor='specifications'>Specifications</FormLabel>
        <Input id='specifications' name='specifications' value={formData.specifications} onChange={handleChange} />
      </FormControl>
      <FormControl isRequired mt={4}>
        <FormLabel htmlFor='price'>Price</FormLabel>
        <Input id='price' name='price' type='number' value={formData.price} onChange={handleChange} />
      </FormControl>
      <Button mt={6} colorScheme='blue' type='submit'>{isEditing ? 'Update' : 'Add'} Phone</Button>
    </Box>
  );
};
