import { Box, Button, Text, useToast } from '@chakra-ui/react';
import { DeleteIcon } from '@chakra-ui/icons';
import { formatPhoneData } from '@utilities/FormatPhoneData';
import { usePhones } from '@hooks/usePhones';

export const PhoneCard = ({ phone }) => {
  const { brand, model, specifications, price } = formatPhoneData(phone);
  const { deletePhone } = usePhones();
  const toast = useToast();

  const handleDelete = () => {
    deletePhone(phone.id);
    toast({
      title: 'Phone deleted.',
      description: `You have deleted the ${brand} ${model}.`,
      status: 'success',
      duration: 5000,
      isClosable: true,
    });
  };

  return (
    <Box borderWidth='1px' borderRadius='lg' overflow='hidden' p={4} m={4}>
      <Text fontWeight='bold' fontSize='xl' mb={2}>{brand} {model}</Text>
      <Text mb={2}>{specifications}</Text>
      <Text fontWeight='semibold' mb={4}>{price}</Text>
      <Button leftIcon={<DeleteIcon />} colorScheme='red' onClick={handleDelete}>
        Delete
      </Button>
    </Box>
  );
};