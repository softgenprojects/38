import { Flex, Box, Heading, useDisclosure, Text } from '@chakra-ui/react';
import { PhoneList } from '@components/PhoneList';
import { PhoneForm } from '@components/PhoneForm';
import { AddIcon } from '@chakra-ui/icons';
import { IconButton } from '@chakra-ui/react';
import { Modal, ModalOverlay, ModalContent, ModalHeader, ModalFooter, ModalBody, ModalCloseButton } from '@chakra-ui/react';
import { usePhones } from '@hooks/usePhones';

const MarketplacePage = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { phones, error } = usePhones();

  if (error) {
    return <Text>Error loading phones: {error.message}</Text>;
  }

  return (
    <Box p={[4, 8, 12]}>
      <Flex justifyContent='space-between' alignItems='center' mb={6}>
        <Heading as='h1' size='xl'>Phone Marketplace</Heading>
        <IconButton
          aria-label='Add phone'
          icon={<AddIcon />}
          onClick={onOpen}
          colorScheme='teal'
          size='lg'
          isRound
        />
      </Flex>
      <PhoneList phones={phones} />
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Add a New Phone</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <PhoneForm onClose={onClose} />
          </ModalBody>
          <ModalFooter>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Box>
  );
};

export default MarketplacePage;
