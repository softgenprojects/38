export const formatPhoneData = (phone) => {
  const { brand, model, specifications, price } = phone;
  const formattedSpecifications = Array.isArray(specifications) ? specifications.join(', ') : 'No specifications';
  return {
    brand,
    model,
    specifications: formattedSpecifications,
    price: `$${price.toFixed(2)}`
  };
};